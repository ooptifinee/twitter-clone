import { useRouter } from "next/router";
import { BsTwitter } from "react-icons/bs";

const SidebarLogo = () => {
  const router = useRouter();
  return (
    <div className="flex flex-row items-center">
      <div
        onClick={() => router.push("/")}
          className="
            lg:hidden
            rounded-full
            h-14
            w-14
            p-4
            items-center
            justify-center
            hover:bg-blue-300
            hover:bg-opacity-10
            cursor-pointer
            transition
          "
      >
        <BsTwitter className="" size={28} color="white" />
      </div>
      <div
        className="
          hidden
        text-white
          relative
          lg:flex
          items-center
          gap-4
          p-4
          rounded-full
          hover:bg-slate-300
          hover:bg-opacity-10
          cursor-pointer
        "
      >
        <BsTwitter size={28} color="white" />
        <h2 className="font-extrabold text-xl">Twitter</h2>
      </div>
    </div>
  );
};

export default SidebarLogo;
